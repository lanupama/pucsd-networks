-module(safe).
-export([start/1]).


start(Code) ->  register(safe,self()),
                wait_for_event(Code,closed,[]).
    

%%Register client process or the process which sends the event as client for testing purposes

%% Check whether case covers all possible conditions. What happens if not 
wait_for_event(Code,closed,KeySoFar) ->
            receive X ->
		    NewKeySoFar = KeySoFar ++ [X],
		    case NewKeySoFar  of
			Code        -> client ! opened,  
			               wait_for_event(Code,open,[]);
			Incomplete when length(Incomplete) < length(Code)
			            -> wait_for_event(Code,closed,NewKeySoFar);
			_           -> wait_for_event(Code,closed,[])
		    end
	    end;
wait_for_event(Code,open,KeySoFar) ->
	    receive 
		 X  ->                                 %% what happens if these 
		    wait_for_event(Code,open,KeySoFar) %% 2 lines are commented
	    after 5000 ->
                    client ! closed,
		    wait_for_event(Code,closed,KeySoFar)
	    end.
		     
