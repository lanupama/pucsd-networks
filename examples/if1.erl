-module(if1).
-export([relative1/1]).

relative1({X,Y}) ->
    if 
	X < Y -> smaller;
	X > Y -> bigger;
	true  -> equal
    end.
	     
%% function_clause, case_clause, if_clause, badarith, badarg, undef
