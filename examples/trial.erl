-module(trial).
-export([myFunc/1,divBy/1]).

myFunc(X) ->
    try
	if X > 0 -> X;
	   true  -> throw(hello)
        end
    catch
        throw:Y -> io:format("My exception is ~w~n",[Y])
    end.

divBy(X) ->
    try 
      case (5/X) of
        Y ->  Y
      end
    catch
       error:Z -> io:format("Except is ~w~n",[Z])
    end. 
