-module(guard1).
-export([lage1/1,lage2/1,lage3/1,lage4/1]).
% This is just for demonstration. Logically unsound as it makes people 
% greater than 80 or 50 minors :)

%Pattern matching 
lage1(X) when X >= 18, X =< 80 
                 -> major;
lage1(_) -> minor.

%Using an if clause
lage2(X) ->
    if 
      X >=18, X=< 50 -> major;
      true           -> minor 
    end.

%Using case
lage3(X) ->
    case ((X>=18) and (X =<80)) of
        true  ->  major;
	_     ->  minor
    end. 

lage4(X) ->
    case X of
	_ when X>=18, X=<80 -> major;
	_                   -> minor
    end.
	    
	       

		      
	    
    

