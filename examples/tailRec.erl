-module(tailRec).
-export([length1/2]).

length1([],Acc)    -> Acc;
length1([_|T],Acc) -> length1(T,Acc+1).
