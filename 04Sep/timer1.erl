-module(timer1).
-export([start/0,send/2]).

%% This function is not required really
start() ->
     init().

init() ->
    case timer:apply_after(20000,timer1,send,[self(),hello]) of
    	{ok,_} -> loop();
       _      -> error
    end.

loop() ->
    receive X ->
        io:format("recd ~p~n",[X])
    after 0  ->
        io:format("recd none ~n"),
	loop()
    end.


send(Sender,Mesg) ->
    Sender ! Mesg.
