-module(receiver).

%% Only has exported functions
-export([start/1,send_ack_reminder/0]).

-record(rData, {capacity
		    ,sequence
		    ,min
		    ,max
		    ,buffer
		    ,ack_interval
		    ,mesg_per_sec
		    }).

start(Capacity) ->  init(Capacity).
     

init(Capacity) ->   register(receiver,self()),
                    loop(listen,initialize_dataState(Capacity,0,0)).

  
loop(FsmState, RData) ->
    receive X ->
	    {NewFsmState,NewRData} =
		case X of
		    ack_reminder -> sender ! {ack,{ RData#rData.buffer
						   ,RData#rData.min
						   ,RData#rData.max
						  }},
				    {FsmState, RData};
		    _         -> handle_mesg(FsmState,RData,X)
                end,
	    loop(NewFsmState,NewRData)
    end.
	     

%% Assume message protocol format is {tag,Data}

%%Initial listening State
handle_mesg(listen,RData,{connection,hello}) ->  sender ! {connSuccess,RData#rData.capacity} ,
						       {connected , RData};
						       

%% Connected State, needs a sequence number
handle_mesg(connected,RData,{newSeq,Sequence}) when RData#rData.min == 0 ->
					   sender !   seqAck,
					   {dataRecv,  RData#rData{sequence = Sequence}};

%% Data Receiving state.

%% First time Data is Recd in the new window
handle_mesg(dataRecv,#rData{min=Min,max=Max,capacity=Capacity,buffer=Buffer,sequence=Sequence,ack_interval=Ack_interval}=RData,{data,Data}) when Min == 0, Data >= Sequence ->
                                            if (Ack_interval == 0) -> 
					            sender ! firstdata,
					            timer:apply_interval(5000, receiver, send_ack_reminder, [])
					    end,
                                            {dataRecv,RData#rData{min=Data,max=Data,buffer = orddict:append(Data,Data,Buffer),ack_interval=5}};

            
%% Check if the data is within capacity and greater than Tail , make it the new Tail
handle_mesg(dataRecv,#rData{min=Min,max=Max,capacity=Capacity,buffer=Buffer,sequence=Sequence}=RData,{data,Data}) when Data > Max, Data =< Capacity + Sequence - 1  ->
					    {dataRecv,RData#rData{max = Data,buffer = orddict:append(Data,Data,Buffer)}}; 
					       
%% Check if the data is before Head 
handle_mesg(dataRecv,#rData{min=Min,max=Max,capacity=Capacity,buffer=Buffer,sequence=Sequence}=RData,{data,Data}) when Data >= Sequence, Data < Min ->
					    {dataRecv,RData#rData{min=Data,buffer= orddict:append(Data,Data,Buffer)}}; 

%% check if the data can be accomodated in recd buffer
handle_mesg(dataRecv,#rData{min=Min,max=Max,capacity=Capacity,buffer=Buffer}=RData,{data,Data}) when Data >= Min, Data =< Max ->
					    {dataRecv,RData#rData{buffer= orddict:append(Data,Data,Buffer)}}; 

handle_mesg(F,D,_) -> {F,D}.

							
%% One example of a timer
start_timer(MilliSec) ->
     spawn(fun() ->
		   receive
		   after MilliSec ->
		       receiver ! dataAck
		   end
	    end).

initialize_dataState(Capacity,Min,Max) ->
           #rData{capacity = Capacity
		    ,sequence = 0
		    ,min = Min
		    ,max = Max
		    ,buffer = orddict:new()
		    ,ack_interval = 0
		    ,mesg_per_sec = 0
		   }.
		    
is_seq_complete(#rData{buffer=Dict,sequence=Sequence,max=Max}) ->
     L = lists:map(fun({K,_})  -> K end, orddict:to_list(Dict)),
     try lists:zipwith(fun({X,_},Y) -> X==Y end 
		                 ,L
				,lists:seq(Sequence,Max)) of
         L1 -> lists:foldl (fun(X,Y) -> X and Y end
			    ,true
			    ,L1)
     catch		 
         _:_ -> false
     end.

send_ack_reminder() -> receiver ! ack_reminder.
     
	     

%%Points
%% How do I represent the buffer? Feel Free to follow your own implementation
%% What I send back as ack depends on my data structure
%% What happens if SeqAck is not received by sender - 
%% How do I create lost packets situation 
