-module(timer2).
-export([start/1,send/2]).

%% This function is not required really
start(Interval) ->
     init(Interval).

init(Interval) ->
    case timer:apply_interval(Interval,timer2,send,[self(),hello]) of
         {ok,TRef} -> timer:apply_after(5*Interval,timer2,send,[self(),stop]),
	              loop(TRef);
	 _         -> error
    end.

loop(TRef) ->
    receive X ->
      case X of 
       hello -> io:format("recd ~p~n",[X]),
                loop(TRef);
       stop  -> timer:cancel(TRef)
      end
    end.	    

send(Sender,Mesg) ->
    Sender ! Mesg.

